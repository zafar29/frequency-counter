var letterCounts = [];
var wordsCounts = [];

function frequencyCount() {
    document.getElementById("lettersDiv").innerHTML = '';
    document.getElementById("wordsDiv").innerHTML = '';
    let typedText = document.getElementById("textInput").value;
    typedText = typedText.toLowerCase();
    typedText = typedText.replace(/[^a-z'\s]+/g, "");
    countLettersFrequency(typedText);
    showLettersFrequency();
    countWordsFrequency(typedText);
    showWordsFrequency();

}

function countLettersFrequency(typedText) {
    let ChaAsArr = typedText.split('');
    for(let currentLetter in ChaAsArr) {
        if(ChaAsArr[currentLetter] !== " ") {
            if(letterCounts[ChaAsArr[currentLetter]] === undefined) {
                letterCounts[ChaAsArr[currentLetter]] = 1;
            } else {
                letterCounts[ChaAsArr[currentLetter]]++;
            }
        }
    }    
}

function countWordsFrequency(typedText) {
    let wordAsArr = typedText.split(' ');
    for(let word in wordAsArr) {
        if(wordAsArr[word] !== " ") {
            if(wordsCounts[wordAsArr[word]] === undefined) {
                wordsCounts[wordAsArr[word]] = 1;
            } else {
                wordsCounts[wordAsArr[word]]++;
            }
        }
    }   

}

function showLettersFrequency() {
    for (let letter in letterCounts) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ");
        span.appendChild(textContent);
        document.getElementById("lettersDiv").appendChild(span);
    }
    letterCounts = [];
}

function showWordsFrequency() {
    for (let word in wordsCounts) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + word + "\": " + wordsCounts[word] + ", ");
        span.appendChild(textContent);
        document.getElementById("wordsDiv").appendChild(span);
    }
    wordsCounts = [];
}
